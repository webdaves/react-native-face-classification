package com.face_classification;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.module.annotations.ReactModule;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@ReactModule(name = FaceClassificationModule.NAME)
public class FaceClassificationModule extends ReactContextBaseJavaModule implements LifecycleEventListener {
  public static final String NAME = "FaceClassification";
  private CameraController cameraController;

  public FaceClassificationModule(ReactApplicationContext reactContext) {
    super(reactContext);
    reactContext.addLifecycleEventListener(this);
  }

  @Override
  @NonNull
  public String getName() {
    return NAME;
  }

  @ReactMethod
  public void startCamera() {
    if (cameraController != null) {
      cameraController.startCamera();
    }
  }

  @ReactMethod
  public void classify(Promise promise) {
    cameraController.takePhoto(new ImageCapture.OnImageCapturedCallback() {
      @Override
      public void onCaptureSuccess(@NonNull ImageProxy image) {
        super.onCaptureSuccess(image);
        FaceClassificationModule.this.processFace(image, promise);
        cameraController.stopCamera();
      }

      @Override
      public void onError(@NonNull ImageCaptureException exception) {
        super.onError(exception);
        cameraController.stopCamera();
        promise.reject("Photo capture failed", exception);
      }
    });
  }

  @Override
  public void onHostResume() {
    if (cameraController == null) {
      cameraController = new CameraController(this.getReactApplicationContext());
    }
  }

  @Override
  public void onHostPause() {
    if (cameraController != null) {
      cameraController.stopCamera();
    }
  }

  @Override
  public void onHostDestroy() {
    if (cameraController != null) {
      cameraController.stopCamera();
    }
  }

  private void processFace(ImageProxy capturedImage, Promise promise) {
    FaceDetectorOptions options =
      new FaceDetectorOptions.Builder()
        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
        .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_NONE)
        .setContourMode(FaceDetectorOptions.LANDMARK_MODE_NONE)
        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
        .build();
    int rotationDegrees = cameraController.getRotationDegrees() != null ? cameraController.getRotationDegrees() : 0;
    @SuppressLint("UnsafeOptInUsageError") InputImage image = InputImage.fromMediaImage(Objects.requireNonNull(capturedImage.getImage()), rotationDegrees);
    FaceDetector detector = FaceDetection.getClient(options);

    detector.process(image)
      .addOnSuccessListener(
        faces -> {
          // Task completed successfully
          List<Float> smileProbs = new ArrayList<>();
          for (Face face : faces) {
            // If classification was enabled:
            if (face.getSmilingProbability() != null) {
              float smileProb = face.getSmilingProbability();
              smileProbs.add(smileProb);
            }
          }
          WritableArray result = Arguments.fromList(smileProbs);
          promise.resolve(result);
        })
      .addOnFailureListener(
        e -> {
          // Task failed with an exception
          promise.reject("Process classification error", e);
        })
      .addOnCompleteListener(task -> capturedImage.close());
  }
}
