package com.face_classification;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.react.bridge.ReactContext;
import com.google.common.util.concurrent.ListenableFuture;

public class CameraController {
  private static final int REQUEST_CODE_PERMISSIONS = 10;
  private static final String TAG = "CAMERA_CONTROLLER";
  private final String[] REQUIRED_PERMISSIONS = {Manifest.permission.CAMERA};
  private final Context context;
  private final ImageCapture imageCapture;
  private ProcessCameraProvider cameraProvider;
  private Integer rotationDegrees;
  private Camera camera;

  CameraController(Context context) {
    this.context = context;
    imageCapture = new ImageCapture.Builder().build();
    // Request camera permissions
    if (!allPermissionsGranted()) {
      ActivityCompat.requestPermissions(
        ((ReactContext) this.context).getCurrentActivity(), REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
    }
  }

  public void takePhoto(ImageCapture.OnImageCapturedCallback callback) {
    // Get a stable reference of the modifiable image capture use case
    Log.d(TAG, "INIT IMAGE CAPTURE!!!");
    ImageCapture imageCapture = this.imageCapture;
    if (imageCapture != null) {
      Log.d(TAG, "BEFORE TAKE PICTURE!!!");
      // Set up image capture listener, which is triggered after photo has
      // been taken
      imageCapture.takePicture(ContextCompat.getMainExecutor(this.context), callback);
      Log.d(TAG, "AFTER TAKE PICTURE!!!");
      return;
    }
    Log.d(TAG, "IMAGE CAPTURE NULL!!!");
  }

  public void startCamera() {
    ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this.context);

    cameraProviderFuture.addListener(() -> {
      try {
        // Used to bind the lifecycle of cameras to the lifecycle owner
        cameraProvider = cameraProviderFuture.get();

        // Select back camera as a default
        CameraSelector cameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA;

        // Unbind use cases before rebinding
        cameraProvider.unbindAll();

        AppCompatActivity activity = ((AppCompatActivity) ((ReactContext) this.context).getCurrentActivity());

        ImageAnalysis imageAnalysis =
          new ImageAnalysis.Builder()
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
            .build();

        imageAnalysis.setAnalyzer(ContextCompat.getMainExecutor(CameraController.this.context), image -> rotationDegrees = image.getImageInfo().getRotationDegrees());

        // Bind use cases to camera
        camera = cameraProvider.bindToLifecycle(
          activity, cameraSelector, imageCapture);

      } catch (Exception exc) {
        Log.e(TAG, "Use case binding failed", exc);
      }

    }, ContextCompat.getMainExecutor(this.context));
  }

  public void stopCamera() {
    if (cameraProvider != null) {
      cameraProvider.unbindAll();
    }
  }

  public Integer getRotationDegrees() {
    return this.rotationDegrees;
  }

  public Camera getCamera() {
    return this.camera;
  }

  private boolean allPermissionsGranted() {
    boolean granted = false;
    for (String permission : REQUIRED_PERMISSIONS) {
      granted = granted || ContextCompat.checkSelfPermission(this.context, permission) == PackageManager.PERMISSION_GRANTED;
    }
    return granted;
  }
}
