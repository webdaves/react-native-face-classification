import { NativeModules, Platform } from 'react-native';

const LINKING_ERROR =
  `The package 'react-native-face-classification' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo managed workflow\n';

const FaceClassification = NativeModules.FaceClassification
  ? NativeModules.FaceClassification
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

export function startCamera(): void {
  return FaceClassification.startCamera();
}

export function classify(): Promise<Array<number>> {
  return FaceClassification.classify();
}
